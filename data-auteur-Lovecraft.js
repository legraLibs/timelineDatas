/*!************************************************
// data-auteur-Lovecraft.js
// auteur:pascal TOLEDO
// site: http://www.legral.fr
// date: 20 septembre 2013
// license: GPLv3
// donnee sur l'auteur: Lovecraft
*/
Lovecraft_evt=new Array();
Lovecraft_evt['naissance']={'titre':'Lovecraft naissance-Deces','date_deb':{'an':1890,'ms':8,'jr':20},'date_fin':{'an':1937,'ms':3,'jr':15}};
Lovecraft_evt['naissance'].texte=Lovecraft_evt['naissance'].titre;
//-----------------------------------------------------
Lovecraft_livres=new Array();
Lovecraft_livres[1]= {'titre':'Le Témoignage de Randolph Carter (1919)','date_deb':{'an':1919},'texte':'Le Témoignage de Randolph Carter'};
Lovecraft_livres[17]= {'titre':'Dagon(1917)','date_deb':{'an':1917}};
Lovecraft_livres[18]={'titre':'Herbert West,réanimateur(1922)','date_deb':{'an':1922}};
Lovecraft_livres[2]={'titre':'La maison maudite(1924)','date_deb':{'an':1924}};
Lovecraft_livres[3]={'titre':'La cle d\'argent(1926)','date_deb':{'an':1926}};
Lovecraft_livres[4]={'titre':'Je suis d\'ailleurs(1926)','date_deb':{'an':1926}};
Lovecraft_livres[5]={'titre':'L\'Appel de Cthulhu(1926)','date_deb':{'an':1926}};
Lovecraft_livres[6]={'titre':'La Couleur tombée du ciel(1927)','date_deb':{'an':1927}};
Lovecraft_livres[7]={'titre':'La Quête onirique de Kadath l\'inconnue(1927)','date_deb':{'an':1927}};
Lovecraft_livres[8]={'titre':'L\'Abomination de Dunwich(1928)','date_deb':{'an':1928}};
Lovecraft_livres[9]={'titre':'L\'Affaire Charles Dexter Ward(1928)','date_deb':{'an':1928}};
Lovecraft_livres[10]={'titre':'L\'Affaire Charles Dexter Ward(1928)','date_deb':{'an':1928}};
Lovecraft_livres[11]={'titre':'Celui qui chuchotait dans les ténèbres(1930)','date_deb':{'an':1930}};
Lovecraft_livres[12]={'titre':'Les Montagnes hallucinées(1931)','date_deb':{'an':1931}};
Lovecraft_livres[13]={'titre':'La Maison de la sorcière(1932)','date_deb':{'an':1932}};
Lovecraft_livres[14]={'titre':'Le Cauchemar d\'Innsmouth (1932)','date_deb':{'an':1932}};
Lovecraft_livres[15]={'titre':'À travers les portes de la clé d\'argent (1933)','date_deb':{'an':1933}};
Lovecraft_livres[16]={'titre':'Dans l\'abîme du temps(1935)','date_deb':{'an':1935}};
