Glaciations.Style=[];
Glaciations.Style['glaciaire']={backgroundColor:'blue'};
Glaciations.Style['interglaciaire']={backgroundColor:'#4D4'};


eresGeologique.evtStyle=new Array();
eresGeologique.evtStyle.backgroundColor=new Array();
	// == superEons == //
	// == Eons == //
eresGeologique.evtStyle.backgroundColor['Hadeen']='#702';
eresGeologique.evtStyle.backgroundColor['Archeen']='#F22';
eresGeologique.evtStyle.backgroundColor['Proterozoique']='#F55';
eresGeologique.evtStyle.backgroundColor['Phanerozoique']='#F99';
	// == Eres == //
eresGeologique.evtStyle.backgroundColor['Eoarcheen']='#F20';
eresGeologique.evtStyle.backgroundColor['Paleoarcheen']='#F24';
eresGeologique.evtStyle.backgroundColor['Pesoarcheen']='#F28';
eresGeologique.evtStyle.backgroundColor['Neoarcheen']='#F2B';

eresGeologique.evtStyle.backgroundColor['Paleo']='#F50';
eresGeologique.evtStyle.backgroundColor['Meso']='#F58';
eresGeologique.evtStyle.backgroundColor['Neo']='#F5F';

eresGeologique.evtStyle.backgroundColor['Paleozoique']='#F59';
eresGeologique.evtStyle.backgroundColor['Cenozoique']='#F5C';

	// == Periodes == //
eresGeologique.evtStyle.backgroundColor['Siderien']='#55F';
eresGeologique.evtStyle.backgroundColor['Rhyacien']='#F22';
eresGeologique.evtStyle.backgroundColor['Orosirien']='#55F';
eresGeologique.evtStyle.backgroundColor['Statherien']='#F22';

eresGeologique.evtStyle.backgroundColor['Calymmien']='#55F';
eresGeologique.evtStyle.backgroundColor['Ectasien']='#F22';
eresGeologique.evtStyle.backgroundColor['Stenien']='#55F';

eresGeologique.evtStyle.backgroundColor['Tonien']='#F22';
eresGeologique.evtStyle.backgroundColor['Cryogemmien']='#55F';
eresGeologique.evtStyle.backgroundColor['Ediacarien']='#F22';

eresGeologique.evtStyle.backgroundColor['Cambrien']='#55F';
eresGeologique.evtStyle.backgroundColor['Ordovicien']='#F22';
eresGeologique.evtStyle.backgroundColor['Silurien']='#55F';
eresGeologique.evtStyle.backgroundColor['Devonien']='#F22';
eresGeologique.evtStyle.backgroundColor['Carbonifere']='#55F';
eresGeologique.evtStyle.backgroundColor['Permien']='#F22';
eresGeologique.evtStyle.backgroundColor['Trias']='#55F';
eresGeologique.evtStyle.backgroundColor['Jurrasique']='#F22';
eresGeologique.evtStyle.backgroundColor['Cretace']='#55F';
eresGeologique.evtStyle.backgroundColor['Paleogene']='#F22';
eresGeologique.evtStyle.backgroundColor['Neogene']='#55F';
eresGeologique.evtStyle.backgroundColor['Quaternaire']='#F22';

	// == Epoques == //
eresGeologique.evtStyle.backgroundColor['Sturtien']='#F22';
eresGeologique.evtStyle.backgroundColor['Varangien']='#55F';
eresGeologique.evtStyle.backgroundColor['Terreneuvien']='#F22';
eresGeologique.evtStyle.backgroundColor['Serie2']='#55F';
eresGeologique.evtStyle.backgroundColor['Serie3']='#F22';
eresGeologique.evtStyle.backgroundColor['Furongien']='#55F';
eresGeologique.evtStyle.backgroundColor['OrdovicienMoyen']='#F22';
eresGeologique.evtStyle.backgroundColor['OrdovicienSuperieur']='#55F';
eresGeologique.evtStyle.backgroundColor['Llandovery']='#F22';
eresGeologique.evtStyle.backgroundColor['Wenlock']='#55F';
eresGeologique.evtStyle.backgroundColor['Ludlow']='#F22';

eresGeologique.evtStyle.backgroundColor['DevonienInferieur']='#55F';
eresGeologique.evtStyle.backgroundColor['DevonienMoyen']='#F22';
eresGeologique.evtStyle.backgroundColor['DevonienSuperieur']='#55F';

eresGeologique.evtStyle.backgroundColor['Dinantien']='#F22';
eresGeologique.evtStyle.backgroundColor['Cisuralien']='#55F';
eresGeologique.evtStyle.backgroundColor['Guadalupien']='#F22';
eresGeologique.evtStyle.backgroundColor['Lopingien']='#55F';
eresGeologique.evtStyle.backgroundColor['TriasInferieur']='#F22';
eresGeologique.evtStyle.backgroundColor['TriasMoyen']='#55F';
eresGeologique.evtStyle.backgroundColor['TriasSuperieur']='#F22';

eresGeologique.evtStyle.backgroundColor['Lias']='#F22';
eresGeologique.evtStyle.backgroundColor['Dogger']='#55F';
eresGeologique.evtStyle.backgroundColor['Malm']='#F22';
	
eresGeologique.evtStyle.backgroundColor['CretaceInferieur']='#55F';
eresGeologique.evtStyle.backgroundColor['CretaceSuperieur']='#F22';
	
	
eresGeologique.evtStyle.backgroundColor['Paleocene']='#55F';
eresGeologique.evtStyle.backgroundColor['Eocene']='#F22';
eresGeologique.evtStyle.backgroundColor['Oligocene']='#55F';
eresGeologique.evtStyle.backgroundColor['Miocene']='#F22';
eresGeologique.evtStyle.backgroundColor['Pliocene']='#55F';

eresGeologique.evtStyle.backgroundColor['Pleistocerene']='#F22';
eresGeologique.evtStyle.backgroundColor['Holocene']='#55F';

	// == Etages == //
eresGeologique.evtStyle.backgroundColor['Fortunien']='#F22';
eresGeologique.evtStyle.backgroundColor['Etage2']='#55F';
eresGeologique.evtStyle.backgroundColor['Etage3']='#F22';
eresGeologique.evtStyle.backgroundColor['Etage4']='#55F';
eresGeologique.evtStyle.backgroundColor['Etage5']='#F22';
eresGeologique.evtStyle.backgroundColor['Drumien']='#55F';
eresGeologique.evtStyle.backgroundColor['Guzhangien']='#F22';
eresGeologique.evtStyle.backgroundColor['Paibien']='#55F';
eresGeologique.evtStyle.backgroundColor['Jiangshanien']='#F22';
eresGeologique.evtStyle.backgroundColor['Etage10']='#55F';
eresGeologique.evtStyle.backgroundColor['Tremadocien']='#F22';
eresGeologique.evtStyle.backgroundColor['Floien']='#55F';
eresGeologique.evtStyle.backgroundColor['Sandbien']='#F22';
eresGeologique.evtStyle.backgroundColor['Katien']='#55F';
eresGeologique.evtStyle.backgroundColor['Hirnantien']='#F22';
eresGeologique.evtStyle.backgroundColor['Rhuddanien']='#55F';
eresGeologique.evtStyle.backgroundColor['Aeronien']='#F22';
eresGeologique.evtStyle.backgroundColor['Telychien']='#55F';
eresGeologique.evtStyle.backgroundColor['Sheinwoodien']='#F22';
eresGeologique.evtStyle.backgroundColor['Gorstien']='#55F';
eresGeologique.evtStyle.backgroundColor['Ludfordien']='#F22';
	
eresGeologique.evtStyle.backgroundColor['Lochkovien']='#55F';
eresGeologique.evtStyle.backgroundColor['Fortunien']='#F22';
eresGeologique.evtStyle.backgroundColor['Praguien']='#55F';
eresGeologique.evtStyle.backgroundColor['Emsien']='#F22';
eresGeologique.evtStyle.backgroundColor['Eifelien']='#55F';
eresGeologique.evtStyle.backgroundColor['Givetien']='#F22';
eresGeologique.evtStyle.backgroundColor['Famennien']='#55F';
eresGeologique.evtStyle.backgroundColor['Tournaisien']='#F22';
eresGeologique.evtStyle.backgroundColor['Viseen']='#55F';
eresGeologique.evtStyle.backgroundColor['Serpoukhovien']='#F22';
eresGeologique.evtStyle.backgroundColor['Bashkirien']='#55F';
eresGeologique.evtStyle.backgroundColor['Moscovien']='#F22';
eresGeologique.evtStyle.backgroundColor['Kasimovien']='#55F';
eresGeologique.evtStyle.backgroundColor['Gzhelien']='#F22';
	
	
eresGeologique.evtStyle.backgroundColor['Asselien']='#F22';
eresGeologique.evtStyle.backgroundColor['Sakmarien']='#55F';
eresGeologique.evtStyle.backgroundColor['Artinskien']='#F22';
eresGeologique.evtStyle.backgroundColor['Kungurien']='#55F';
eresGeologique.evtStyle.backgroundColor['Roadien']='#F22';
eresGeologique.evtStyle.backgroundColor['Wordien']='#55F';
eresGeologique.evtStyle.backgroundColor['Capitanien']='#F22';
eresGeologique.evtStyle.backgroundColor['Wuchiapingien']='#55F';
eresGeologique.evtStyle.backgroundColor['Changhsingien']='#F22';

eresGeologique.evtStyle.backgroundColor['Induen']='#55F';
eresGeologique.evtStyle.backgroundColor['Olenekien']='#F22';
eresGeologique.evtStyle.backgroundColor['Anisien']='#55F';
eresGeologique.evtStyle.backgroundColor['Ladinien']='#F22';
eresGeologique.evtStyle.backgroundColor['Carnien']='#55F';
eresGeologique.evtStyle.backgroundColor['Norien']='#F22';
eresGeologique.evtStyle.backgroundColor['Rhetien']='#55F';
