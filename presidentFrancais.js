//*************************************************
// presidentFrancais.js
// (c)pascal TOLEDO http://www.legral.fr
// date 30 juillet 2013
// v1.0: 
// donnee sur les presidents francais
//*************************************************

presidentsFrancais_evt=new Array();
presidentsFrancais_evt[01]={'date_deb':{'an':1848,'ms':12,'jr':20},'date_fin':{'an':1852,'ms':12,'jr':02},'titre':'Louis-Napoléon Bonaparte'};

presidentsFrancais_evt[02]={'date_deb':{'an':1871,'ms':8, 'jr':31},'date_fin':{'an':1873,'ms':05,'jr':24},'titre':'Adolphe Thiers'};
presidentsFrancais_evt[03]={'date_deb':{'an':1873,'ms':05,'jr':24},'date_fin':{'an':1879,'ms':01,'jr':30},'titre':'Patrice de Mac Mahon',};
presidentsFrancais_evt[04]={'date_deb':{'an':1879,'ms':01,'jr':30},'date_fin':{'an':1887,'ms':12,'jr':02},'titre':'Jules Grévy'};
presidentsFrancais_evt[05]={'date_deb':{'an':1887,'ms':12,'jr':03},'date_fin':{'an':1894,'ms':06,'jr':25},'titre':'Sadi Carnot'};
presidentsFrancais_evt[06]={'date_deb':{'an':1894,'ms':06,'jr':27},'date_fin':{'an':1895,'ms':01,'jr':16},'titre':'Jean Casimir-Perier'};
presidentsFrancais_evt[07]={'date_deb':{'an':1895,'ms':01,'jr':17},'date_fin':{'an':1899,'ms':02,'jr':16},'titre':'Félix Faure'};

presidentsFrancais_evt[8] ={'date_deb':{'an':1899,'ms':02,'jr':18},'date_fin':{'an':1906,'ms':06,'jr':18},'titre':'Émile Loubet'};
presidentsFrancais_evt[9] ={'date_deb':{'an':1906,'ms':06,'jr':18},'date_fin':{'an':1913,'ms':02,'jr':18},'titre':'Armand Fallières'};
presidentsFrancais_evt[10]={'date_deb':{'an':1913,'ms':02,'jr':18},'date_fin':{'an':1920,'ms':02,'jr':18},'titre':'Raymond Poincaré'};
presidentsFrancais_evt[11]={'date_deb':{'an':1920,'ms':02,'jr':18},'date_fin':{'an':1920,'ms':9, 'jr':21},'titre':'Paul Deschanel'};
presidentsFrancais_evt[12]={'date_deb':{'an':1920,'ms':8, 'jr':23},'date_fin':{'an':1924,'ms':06,'jr':11},'titre':'Alexandre Millerand'};
presidentsFrancais_evt[13]={'date_deb':{'an':1924,'ms':06,'jr':13},'date_fin':{'an':1931,'ms':06,'jr':13},'titre':'Gaston Doumergue'};
presidentsFrancais_evt[14]={'date_deb':{'an':1931,'ms':06,'jr':13},'date_fin':{'an':1932,'ms':05,'jr':07},'titre':'Paul Doumer'};
presidentsFrancais_evt[15]={'date_deb':{'an':1932,'ms':05,'jr':10},'date_fin':{'an':1940,'ms':07,'jr':11},'titre':'Albert Lebrun'};

presidentsFrancais_evt[16]={'date_deb':{'an':1947,'ms':01,'jr':16},'date_fin':{'an':1954,'ms':01,'jr':16},'titre':'Vincent Auriol'};
presidentsFrancais_evt[17]={'date_deb':{'an':1954,'ms':01,'jr':16},'date_fin':{'an':1959,'ms':01,'jr': 8},'titre':'René Coty'};

presidentsFrancais_evt[18]= {'date_deb':{'an':1959,'ms':01,'jr':8},'date_fin':{'an':1969,'ms':04,'jr':28},'titre':'Charles de Gaulle'};
presidentsFrancais_evt[18.1]={'date_deb':{'an':1969,'ms':04,'jr':28},'date_fin':{'an':1969,'ms':06,'jr':20},'titre':'Alain Poher'};
presidentsFrancais_evt[19]={'date_deb':{'an':1969,'ms':06,'jr':20},'date_fin':{'an':1974,'ms':04,'jr':02},'titre':'Georges Pompidou'};
presidentsFrancais_evt[19.1]={'date_deb':{'an':1974,'ms':04,'jr':02},'date_fin':{'an':1974,'ms':05,'jr':27},'titre':'Alain Poher'};
presidentsFrancais_evt[20]={'date_deb':{'an':1974,'ms':05,'jr':27},'date_fin':{'an':1981,'ms':05,'jr':21},'titre':"Valéry Giscard d'Estaing"};
presidentsFrancais_evt[21]={'date_deb':{'an':1981,'ms':05,'jr':21},'date_fin':{'an':1995,'ms':05,'jr':17},'titre':'François Mitterrand'};
presidentsFrancais_evt[22]={'date_deb':{'an':1995,'ms':05,'jr':17},'date_fin':{'an':2007,'ms':05,'jr':16},'titre':'Jacques Chirac'};
presidentsFrancais_evt[23]={'date_deb':{'an':2007,'ms':05,'jr':16},'date_fin':{'an':2012,'ms':05,'jr':15},'titre':'Nicolas Sarkozy'};
presidentsFrancais_evt[24]={'date_deb':{'an':2012,'ms':05,'jr':15},'date_fin':{'an':2012+4,'ms':05,'jr':15},'titre':'François Hollande'};


for(var i=1;i<presidentsFrancais_evt.length;i++)presidentsFrancais_evt[i].texte=presidentsFrancais_evt[i].titre;
