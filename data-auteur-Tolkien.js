//*************************************************
// auteurs-Lovecraft.js
// (c)pascal TOLEDO http://www.legral.fr
// date 7 janvier 2012
// donnee sur l'auteur: Tolkien
// donnees - auteur: Tolkien

Tolkien_evt=new Array();
Tolkien_evt['naissance']={'titre':'Tolkien naissance-Deces','date_deb':{'an':1892,'ms':1,'jr':3},'date_fin':{'an':1973,'ms':7,'jr':2}};
Tolkien_evt['naissance'].texte=Tolkien_evt['naissance'].titre;
//-----------------------------------------------------
Tolkien_livres=new Array();
Tolkien_livres[1]= {'titre':'Beowulf(1936)','date_deb':{'an':1936}};
Tolkien_livres[2]= {'titre':'Bilbo le Hobbit(1937)','date_deb':{'an':1937}};
Tolkien_livres[3]= {'titre':'Feuille, de Niggle(1945)','date_deb':{'an':1945}};
Tolkien_livres[4]= {'titre':'Du conte de fées(1974)','date_deb':{'an':1974}};
Tolkien_livres[5]= {'titre':'Le Fermier Gilles de Ham(1949)','date_deb':{'an':1949}};
Tolkien_livres[6]= {'titre':"Le Seigneur des anneaux:a Communauté de l'anneau(1954)",'date_deb':{'an':1954}};
Tolkien_livres[7]= {'titre':'Le Seigneur des anneaux:Les Deux Tours(1954)','date_deb':{'an':1954}};
Tolkien_livres[8]= {'titre':'Le Seigneur des anneaux:Le Retour du roi(1955)','date_deb':{'an':1955}};
Tolkien_livres[9]= {'titre':'Les Aventures de Tom Bombadil(1962)','date_deb':{'an':1962}};
Tolkien_livres[10]= {'titre':'Smith de Grand Wootton(1967)','date_deb':{'an':1967}};
Tolkien_livres[11]= {'titre':'Sir Gawain and the Green Knight(1975)','date_deb':{'an':1975}};
Tolkien_livres[12]= {'titre':'Pearl(1975)','date_deb':{'an':1975}};
Tolkien_livres[13]= {'titre':'Sir Orfeo(1975)','date_deb':{'an':1975}};
Tolkien_livres[14]= {'titre':'Le Silmarillion(1977)','date_deb':{'an':1977}};
Tolkien_livres[15]= {'titre':'Contes et légendes inachevés(1980)','date_deb':{'an':1980}};

